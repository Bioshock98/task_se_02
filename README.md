# Task Manager 2.0
## software
+ JRE
+ Java 8
+ Maven 4.0
## developer
Pavel Pyshinskiy

email: pavel.pyshinskiy@gmail.com
## build application
```
mvn clean
mvn install
```
## run application
```
java -jar target/artifactId-version-SNAPSHOT.jar
```
