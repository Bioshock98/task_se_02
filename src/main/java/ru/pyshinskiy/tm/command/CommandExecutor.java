package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.manager.ProjectManager;
import ru.pyshinskiy.tm.manager.TaskManager;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandExecutor {
    private ProjectManager projectManager = new ProjectManager();
    private TaskManager taskManager = new TaskManager();
    private BufferedReader input;

    public CommandExecutor(BufferedReader input) {
        this.input = input;
    }

    public void processCommand(Command command) throws IOException {
        switch (command) {
            case PROJECT_CLEAR:
                projectManager.clearProjects();
                break;
            case PROJECT_CREATE:
                projectManager.createProject(input);
                break;
            case PROJECT_LIST:
                projectManager.listProjects(input);
                break;
            case PROJECT_EDIT:
                projectManager.editProject(input);
                break;
            case PROJECT_REMOVE:
                projectManager.removeProject(input);
                break;
            case TASK_CLEAR:
                taskManager.clearTasks(input);
                break;
            case TASK_CREATE:
                taskManager.createTask(input);
                break;
            case TASK_LIST:
                taskManager.listTask(input);
                break;
            case TASK_EDIT:
                taskManager.editTask(input);
                break;
            case TASK_REMOVE:
                taskManager.removeTask(input);
                break;
            case HELP:
                help();
                break;
        }
    }

    private void help() {
        String help = "help: Show all commands.\n" +
                "project_clear: Remove all projects.\n" +
                "project_create: Create new project.\n" +
                "project_list: Show all projects.\n" +
                "project_edit: Edit selected project\n" +
                "project_remove: Remove selected project\n" +
                "task_clear: Remove all tasks.\n" +
                "task_create: Create new task.\n" +
                "task_list: Show all tasks.\n" +
                "task_edit: Edit selected task.\n" +
                "task_remove: Remove selected task.";
        System.out.println(help);
    }
}
