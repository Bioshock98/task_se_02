package ru.pyshinskiy.tm.command;

public enum Command {
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_REMOVE,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_REMOVE,
    HELP,
    UNKNOW_COMMAND,
    TASK_EDIT, PROJECT_EDIT,
    EXIT
}
