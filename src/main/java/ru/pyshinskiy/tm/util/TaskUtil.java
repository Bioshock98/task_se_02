package ru.pyshinskiy.tm.util;

import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public class TaskUtil {

    public static void printTasks(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }
}