package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.command.Command;
import ru.pyshinskiy.tm.command.CommandExecutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static void main(String[] args) throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        CommandExecutor commandExecutor = new CommandExecutor(input);

        while(true) {
            String commandInput = input.readLine();
            Command command = Command.valueOf(commandInput.toUpperCase());

            if(command.equals(Command.EXIT)) {
                input.close();
                System.exit(1);
            }
            if(command.equals(Command.UNKNOW_COMMAND)) continue;

            commandExecutor.processCommand(command);
        }
    }
}
