package ru.pyshinskiy.tm.manager;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;

import static ru.pyshinskiy.tm.util.ProjectUtil.printProjects;
import static ru.pyshinskiy.tm.util.TaskUtil.printTasks;

public class TaskManager {
    private ProjectManager projectManager = new ProjectManager();

    public void clearTasks(BufferedReader input) throws IOException {
        System.out.println("[ENTER PROJECT ID TO REMOVE ALL TASK]");

        printProjects(projectManager.getProjects(), false);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        Project project = projectManager.getProject(projectId);
        project.getTasks().clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void createTask(BufferedReader input) throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER PROJECT ID TO ADD A NEW TASK]");
        printProjects(projectManager.getProjects(), false);
        final int projectId = Integer.parseInt(input.readLine());
        System.out.println("[ENTER TASK NAME]");
        final Project project = projectManager.getProject(projectId - 1);
        project.getTasks().add(new Task(input.readLine(), project));
        System.out.println("[OK]");
    }

    public void listTask(BufferedReader input) throws IOException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER PROJECT ID");
        final int projectId = Integer.parseInt(input.readLine());
        final Project project = projectManager.getProject(projectId);
        System.out.println("ENTER A TASK ID");
        printTasks(project.getTasks());
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        final Task task = project.getTasks().get(taskId);
        System.out.println(task.getName());
    }

    public void editTask(BufferedReader input) throws IOException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectManager.getProjects(), true);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        final Project project = projectManager.getProject(projectId);

        System.out.println("ENTER TASK ID");
        printTasks(project.getTasks());
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        System.out.println("ENTER NAME");
        String newTaskName = input.readLine();
        String oldTaskName = project.getTasks().get(taskId).getName();
        project.getTasks().get(taskId).setName(newTaskName);
        System.out.println("TASK NAME CHANGED FROM " + oldTaskName + " to " + newTaskName);
    }

    public void removeTask(BufferedReader input) throws IOException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectManager.getProjects(), true);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        final Project project = projectManager.getProject(projectId);
        System.out.println("ENTER TASK ID");
        printTasks(project.getTasks());
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        project.getTasks().remove(taskId);
        System.out.println("[TASK REMOVED]");
    }
}
