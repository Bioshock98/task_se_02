package ru.pyshinskiy.tm.manager;

import ru.pyshinskiy.tm.entity.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.ProjectUtil.printProjects;

public class ProjectManager {
    private static final List<Project> projects = new LinkedList<>();

    List<Project> getProjects() {
        return projects;
    }

    Project getProject(int projectId) {
        return projects.get(projectId);
    }

    public void clearProjects() {
        projects.clear();
        System.out.println("[ALL PROJECT REMOVED]");
    }

    public void createProject(BufferedReader input) throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        projects.add(new Project(input.readLine()));
        System.out.println("[OK]");
    }

    public void listProjects(BufferedReader input) throws IOException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER PROJECT ID OR ENTER \"ALL\" IF YOU WANT ALL PROJECTS");
        printProjects(projects, false);

        if (input.readLine().equals("ALL")) printProjects(projects, true);
        else {
            final int projectId = Integer.parseInt(input.readLine());
            Project project = projects.get(projectId - 1);
            System.out.println((projectId + 1) + "." + " " + project.getName());
        }
    }

    public void editProject(BufferedReader input) throws IOException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projects, false);
        final int projectId = Integer.parseInt(input.readLine());
        System.out.println("ENTER NAME");
        projects.get(projectId).setName(input.readLine());
    }

    public void removeProject(BufferedReader input) throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projects, false);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        projects.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}
