package ru.pyshinskiy.tm.entity;

public class Task {
    private String name;

    private Project project;

    public Task(String name, Project project) {
        this.name = name;
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }
}
