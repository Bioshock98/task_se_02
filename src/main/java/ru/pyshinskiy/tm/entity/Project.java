package ru.pyshinskiy.tm.entity;

import java.util.LinkedList;
import java.util.List;

public class Project {
    private String name;
    private final List<Task> tasks = new LinkedList<>();

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Project) return this.name.equals(((Project) obj).getName());
        return false;
    }
}